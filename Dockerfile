FROM maven:3.6.0-jdk-8-alpine AS MAVEN_BUILD
COPY pom.xml /build/
COPY src /build/src/
WORKDIR /build/
RUN mvn package
FROM openjdk:8-jre-alpine
WORKDIR /app
COPY --from=MAVEN_BUILD /build/target/status-0.0.1-SNAPSHOT.jar /app/
ENTRYPOINT ["java", "-jar", "status-0.0.1-SNAPSHOT.jar"]
EXPOSE 7000
CMD ["java", "-jar", "-b", "0.0.0.0"]