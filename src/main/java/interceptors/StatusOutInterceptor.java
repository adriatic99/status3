package interceptors;

import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.interceptor.LoggingMessage;
import org.apache.cxf.io.CacheAndWriteOutputStream;
import org.apache.cxf.io.CachedOutputStream;
import org.apache.cxf.io.CachedOutputStreamCallback;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;

import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import java.io.OutputStream;

public class StatusOutInterceptor extends AbstractPhaseInterceptor<Message> {

    public StatusOutInterceptor() {
        super(Phase.SETUP);
    }

    @Override
    public void handleMessage(Message message) throws Fault {
        SOAPMessage soapMessage = message.getContent(SOAPMessage.class);
        try {
            String msg = soapMessage.getSOAPBody().getValue();
            System.out.println(msg);
        } catch (SOAPException e) {
            e.printStackTrace();
        }

        System.out.println("message");
    }

    public void handleFault(Message messageParam) {
        System.out.println("fault");
        SOAPMessage soapMessage = messageParam.getContent(SOAPMessage.class);
        try {
            System.out.println(messageParam.getDestination().getAddress().getAddress().getValue());
            String msg = soapMessage.getSOAPBody().getValue();
            System.out.println(msg);
        } catch (SOAPException e) {
            e.printStackTrace();
        }
    }
}
