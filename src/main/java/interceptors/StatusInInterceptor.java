package interceptors;

import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;

public class StatusInInterceptor extends AbstractPhaseInterceptor<Message> {

    public StatusInInterceptor() {
        super(Phase.RECEIVE);
    }

    @Override
    public void handleMessage(Message message) throws Fault {
        System.out.println(message.getId());
    }

    public void handleFault(Message messageParam) {
    }
}
