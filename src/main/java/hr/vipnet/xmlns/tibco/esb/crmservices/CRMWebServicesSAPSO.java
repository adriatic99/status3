package hr.vipnet.xmlns.tibco.esb.crmservices;

import org.apache.cxf.interceptor.InInterceptors;
import org.apache.cxf.interceptor.OutInterceptors;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.Service;

/**
 * This class was generated by Apache CXF 3.3.5
 * 2020-05-13T19:33:24.820+02:00
 * Generated source version: 3.3.5
 *
 */
@WebServiceClient(name = "CRMWebServicesSAPSO",
                  wsdlLocation = "file:CRMWebServicesSAPSO.wsdl",
                  targetNamespace = "http://www.vipnet.hr/xmlns/TIBCO/ESB/CRMServices")
public class CRMWebServicesSAPSO extends Service {

    public final static URL WSDL_LOCATION;

    public final static QName SERVICE = new QName("http://www.vipnet.hr/xmlns/TIBCO/ESB/CRMServices", "CRMWebServicesSAPSO");
    public final static QName CRMWSHTTPPortSAPSO = new QName("http://www.vipnet.hr/xmlns/TIBCO/ESB/CRMServices", "CRMWSHTTPPortSAPSO");
    static {
        URL url = null;
        try {
            url = new URL("file:CRMWebServicesSAPSO.wsdl");
        } catch (MalformedURLException e) {
            java.util.logging.Logger.getLogger(CRMWebServicesSAPSO.class.getName())
                .log(java.util.logging.Level.INFO,
                     "Can not initialize the default wsdl from {0}", "file:CRMWebServicesSAPSO.wsdl");
        }
        WSDL_LOCATION = url;
    }

    public CRMWebServicesSAPSO(URL wsdlLocation) {
        super(wsdlLocation, SERVICE);
    }

    public CRMWebServicesSAPSO(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public CRMWebServicesSAPSO() {
        super(WSDL_LOCATION, SERVICE);
    }

    public CRMWebServicesSAPSO(WebServiceFeature ... features) {
        super(WSDL_LOCATION, SERVICE, features);
    }

    public CRMWebServicesSAPSO(URL wsdlLocation, WebServiceFeature ... features) {
        super(wsdlLocation, SERVICE, features);
    }

    public CRMWebServicesSAPSO(URL wsdlLocation, QName serviceName, WebServiceFeature ... features) {
        super(wsdlLocation, serviceName, features);
    }




    /**
     *
     * @return
     *     returns PortType
     */
    @WebEndpoint(name = "CRMWSHTTPPortSAPSO")
    public PortType getCRMWSHTTPPortSAPSO() {
        return super.getPort(CRMWSHTTPPortSAPSO, PortType.class);
    }

    /**
     *
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns PortType
     */
    @WebEndpoint(name = "CRMWSHTTPPortSAPSO")
    public PortType getCRMWSHTTPPortSAPSO(WebServiceFeature... features) {
        return super.getPort(CRMWSHTTPPortSAPSO, PortType.class, features);
    }

}
