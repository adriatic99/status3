
package hr.vipnet.xmlns.tibco.esb.crmservices;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CreateSalesOrderRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CreateSalesOrderRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="orderNumberSIEBEL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="salesOrderSAP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="intershopPOnumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="remark" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="deliveryNotice" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CustomerData"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="billingAccountName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="subscriberNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="shipToStreet" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="shipToHouseNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="shipToCity" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="shipToPostalCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="legalAddress" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="legalStreet" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="legalCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="legalHouseNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="legalPostalCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="pickupAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="siebelPosition" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="siebelChannel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="paymentDetails" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="MMVarantyCard" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="SIMSerialNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="welcomeLetter" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="paperless" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="businessCondition" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="virtualHomeboxFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="HWData" maxOccurs="unbounded"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="materialNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="model" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="netPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *                   &lt;element name="upFrontCost" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *                   &lt;element name="assetID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="serialNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateSalesOrderRequestType", propOrder = {
    "orderNumberSIEBEL",
    "salesOrderSAP",
    "intershopPOnumber",
    "remark",
    "deliveryNotice",
    "customerData",
    "hwData"
})
@XmlSeeAlso({
    CreateSalesOrderRequest.class
})
public class CreateSalesOrderRequestType {

    protected String orderNumberSIEBEL;
    protected String salesOrderSAP;
    protected String intershopPOnumber;
    protected String remark;
    protected String deliveryNotice;
    @XmlElement(name = "CustomerData", required = true)
    protected CreateSalesOrderRequestType.CustomerData customerData;
    @XmlElement(name = "HWData", required = true)
    protected List<CreateSalesOrderRequestType.HWData> hwData;

    /**
     * Gets the value of the orderNumberSIEBEL property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderNumberSIEBEL() {
        return orderNumberSIEBEL;
    }

    /**
     * Sets the value of the orderNumberSIEBEL property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderNumberSIEBEL(String value) {
        this.orderNumberSIEBEL = value;
    }

    /**
     * Gets the value of the salesOrderSAP property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSalesOrderSAP() {
        return salesOrderSAP;
    }

    /**
     * Sets the value of the salesOrderSAP property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSalesOrderSAP(String value) {
        this.salesOrderSAP = value;
    }

    /**
     * Gets the value of the intershopPOnumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIntershopPOnumber() {
        return intershopPOnumber;
    }

    /**
     * Sets the value of the intershopPOnumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIntershopPOnumber(String value) {
        this.intershopPOnumber = value;
    }

    /**
     * Gets the value of the remark property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemark() {
        return remark;
    }

    /**
     * Sets the value of the remark property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemark(String value) {
        this.remark = value;
    }

    /**
     * Gets the value of the deliveryNotice property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeliveryNotice() {
        return deliveryNotice;
    }

    /**
     * Sets the value of the deliveryNotice property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeliveryNotice(String value) {
        this.deliveryNotice = value;
    }

    /**
     * Gets the value of the customerData property.
     * 
     * @return
     *     possible object is
     *     {@link CreateSalesOrderRequestType.CustomerData }
     *     
     */
    public CreateSalesOrderRequestType.CustomerData getCustomerData() {
        return customerData;
    }

    /**
     * Sets the value of the customerData property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreateSalesOrderRequestType.CustomerData }
     *     
     */
    public void setCustomerData(CreateSalesOrderRequestType.CustomerData value) {
        this.customerData = value;
    }

    /**
     * Gets the value of the hwData property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the hwData property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHWData().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CreateSalesOrderRequestType.HWData }
     * 
     * 
     */
    public List<CreateSalesOrderRequestType.HWData> getHWData() {
        if (hwData == null) {
            hwData = new ArrayList<CreateSalesOrderRequestType.HWData>();
        }
        return this.hwData;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="billingAccountName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="subscriberNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="shipToStreet" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="shipToHouseNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="shipToCity" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="shipToPostalCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="legalAddress" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="legalStreet" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="legalCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="legalHouseNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="legalPostalCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="pickupAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="siebelPosition" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="siebelChannel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="paymentDetails" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="MMVarantyCard" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="SIMSerialNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="welcomeLetter" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="paperless" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="businessCondition" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="virtualHomeboxFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "billingAccountName",
        "subscriberNumber",
        "shipToStreet",
        "shipToHouseNumber",
        "shipToCity",
        "shipToPostalCode",
        "legalAddress",
        "pickupAddress",
        "siebelPosition",
        "siebelChannel",
        "paymentDetails",
        "mmVarantyCard",
        "simSerialNumber",
        "welcomeLetter",
        "paperless",
        "businessCondition",
        "virtualHomeboxFlag"
    })
    public static class CustomerData {

        @XmlElement(required = true)
        protected String billingAccountName;
        @XmlElement(required = true)
        protected String subscriberNumber;
        @XmlElement(required = true)
        protected String shipToStreet;
        @XmlElement(required = true)
        protected String shipToHouseNumber;
        @XmlElement(required = true)
        protected String shipToCity;
        @XmlElement(required = true)
        protected String shipToPostalCode;
        protected CreateSalesOrderRequestType.CustomerData.LegalAddress legalAddress;
        protected String pickupAddress;
        protected String siebelPosition;
        protected String siebelChannel;
        @XmlElement(required = true)
        protected String paymentDetails;
        @XmlElement(name = "MMVarantyCard")
        protected String mmVarantyCard;
        @XmlElement(name = "SIMSerialNumber")
        protected String simSerialNumber;
        protected String welcomeLetter;
        protected String paperless;
        protected String businessCondition;
        protected String virtualHomeboxFlag;

        /**
         * Gets the value of the billingAccountName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBillingAccountName() {
            return billingAccountName;
        }

        /**
         * Sets the value of the billingAccountName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBillingAccountName(String value) {
            this.billingAccountName = value;
        }

        /**
         * Gets the value of the subscriberNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSubscriberNumber() {
            return subscriberNumber;
        }

        /**
         * Sets the value of the subscriberNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSubscriberNumber(String value) {
            this.subscriberNumber = value;
        }

        /**
         * Gets the value of the shipToStreet property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getShipToStreet() {
            return shipToStreet;
        }

        /**
         * Sets the value of the shipToStreet property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setShipToStreet(String value) {
            this.shipToStreet = value;
        }

        /**
         * Gets the value of the shipToHouseNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getShipToHouseNumber() {
            return shipToHouseNumber;
        }

        /**
         * Sets the value of the shipToHouseNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setShipToHouseNumber(String value) {
            this.shipToHouseNumber = value;
        }

        /**
         * Gets the value of the shipToCity property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getShipToCity() {
            return shipToCity;
        }

        /**
         * Sets the value of the shipToCity property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setShipToCity(String value) {
            this.shipToCity = value;
        }

        /**
         * Gets the value of the shipToPostalCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getShipToPostalCode() {
            return shipToPostalCode;
        }

        /**
         * Sets the value of the shipToPostalCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setShipToPostalCode(String value) {
            this.shipToPostalCode = value;
        }

        /**
         * Gets the value of the legalAddress property.
         * 
         * @return
         *     possible object is
         *     {@link CreateSalesOrderRequestType.CustomerData.LegalAddress }
         *     
         */
        public CreateSalesOrderRequestType.CustomerData.LegalAddress getLegalAddress() {
            return legalAddress;
        }

        /**
         * Sets the value of the legalAddress property.
         * 
         * @param value
         *     allowed object is
         *     {@link CreateSalesOrderRequestType.CustomerData.LegalAddress }
         *     
         */
        public void setLegalAddress(CreateSalesOrderRequestType.CustomerData.LegalAddress value) {
            this.legalAddress = value;
        }

        /**
         * Gets the value of the pickupAddress property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPickupAddress() {
            return pickupAddress;
        }

        /**
         * Sets the value of the pickupAddress property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPickupAddress(String value) {
            this.pickupAddress = value;
        }

        /**
         * Gets the value of the siebelPosition property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSiebelPosition() {
            return siebelPosition;
        }

        /**
         * Sets the value of the siebelPosition property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSiebelPosition(String value) {
            this.siebelPosition = value;
        }

        /**
         * Gets the value of the siebelChannel property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSiebelChannel() {
            return siebelChannel;
        }

        /**
         * Sets the value of the siebelChannel property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSiebelChannel(String value) {
            this.siebelChannel = value;
        }

        /**
         * Gets the value of the paymentDetails property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPaymentDetails() {
            return paymentDetails;
        }

        /**
         * Sets the value of the paymentDetails property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPaymentDetails(String value) {
            this.paymentDetails = value;
        }

        /**
         * Gets the value of the mmVarantyCard property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMMVarantyCard() {
            return mmVarantyCard;
        }

        /**
         * Sets the value of the mmVarantyCard property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMMVarantyCard(String value) {
            this.mmVarantyCard = value;
        }

        /**
         * Gets the value of the simSerialNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSIMSerialNumber() {
            return simSerialNumber;
        }

        /**
         * Sets the value of the simSerialNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSIMSerialNumber(String value) {
            this.simSerialNumber = value;
        }

        /**
         * Gets the value of the welcomeLetter property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getWelcomeLetter() {
            return welcomeLetter;
        }

        /**
         * Sets the value of the welcomeLetter property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setWelcomeLetter(String value) {
            this.welcomeLetter = value;
        }

        /**
         * Gets the value of the paperless property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPaperless() {
            return paperless;
        }

        /**
         * Sets the value of the paperless property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPaperless(String value) {
            this.paperless = value;
        }

        /**
         * Gets the value of the businessCondition property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBusinessCondition() {
            return businessCondition;
        }

        /**
         * Sets the value of the businessCondition property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBusinessCondition(String value) {
            this.businessCondition = value;
        }

        /**
         * Gets the value of the virtualHomeboxFlag property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVirtualHomeboxFlag() {
            return virtualHomeboxFlag;
        }

        /**
         * Sets the value of the virtualHomeboxFlag property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVirtualHomeboxFlag(String value) {
            this.virtualHomeboxFlag = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="legalStreet" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="legalCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="legalHouseNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="legalPostalCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "legalStreet",
            "legalCity",
            "legalHouseNumber",
            "legalPostalCode"
        })
        public static class LegalAddress {

            protected String legalStreet;
            protected String legalCity;
            protected String legalHouseNumber;
            protected String legalPostalCode;

            /**
             * Gets the value of the legalStreet property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLegalStreet() {
                return legalStreet;
            }

            /**
             * Sets the value of the legalStreet property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLegalStreet(String value) {
                this.legalStreet = value;
            }

            /**
             * Gets the value of the legalCity property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLegalCity() {
                return legalCity;
            }

            /**
             * Sets the value of the legalCity property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLegalCity(String value) {
                this.legalCity = value;
            }

            /**
             * Gets the value of the legalHouseNumber property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLegalHouseNumber() {
                return legalHouseNumber;
            }

            /**
             * Sets the value of the legalHouseNumber property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLegalHouseNumber(String value) {
                this.legalHouseNumber = value;
            }

            /**
             * Gets the value of the legalPostalCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLegalPostalCode() {
                return legalPostalCode;
            }

            /**
             * Sets the value of the legalPostalCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLegalPostalCode(String value) {
                this.legalPostalCode = value;
            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="materialNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="model" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="netPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
     *         &lt;element name="upFrontCost" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
     *         &lt;element name="assetID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="serialNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "materialNumber",
        "model",
        "netPrice",
        "upFrontCost",
        "assetID",
        "serialNumber"
    })
    public static class HWData {

        @XmlElement(required = true)
        protected String materialNumber;
        @XmlElement(required = true)
        protected String model;
        protected BigDecimal netPrice;
        protected BigDecimal upFrontCost;
        protected String assetID;
        protected String serialNumber;

        /**
         * Gets the value of the materialNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMaterialNumber() {
            return materialNumber;
        }

        /**
         * Sets the value of the materialNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMaterialNumber(String value) {
            this.materialNumber = value;
        }

        /**
         * Gets the value of the model property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getModel() {
            return model;
        }

        /**
         * Sets the value of the model property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setModel(String value) {
            this.model = value;
        }

        /**
         * Gets the value of the netPrice property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getNetPrice() {
            return netPrice;
        }

        /**
         * Sets the value of the netPrice property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setNetPrice(BigDecimal value) {
            this.netPrice = value;
        }

        /**
         * Gets the value of the upFrontCost property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getUpFrontCost() {
            return upFrontCost;
        }

        /**
         * Sets the value of the upFrontCost property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setUpFrontCost(BigDecimal value) {
            this.upFrontCost = value;
        }

        /**
         * Gets the value of the assetID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAssetID() {
            return assetID;
        }

        /**
         * Sets the value of the assetID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAssetID(String value) {
            this.assetID = value;
        }

        /**
         * Gets the value of the serialNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSerialNumber() {
            return serialNumber;
        }

        /**
         * Sets the value of the serialNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSerialNumber(String value) {
            this.serialNumber = value;
        }

    }

}
