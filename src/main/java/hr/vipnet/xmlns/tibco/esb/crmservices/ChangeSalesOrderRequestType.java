
package hr.vipnet.xmlns.tibco.esb.crmservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ChangeSalesOrderRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ChangeSalesOrderRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="salesOrderNumberSAP" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="orderNumberSIEBEL" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="remark" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChangeSalesOrderRequestType", propOrder = {
    "salesOrderNumberSAP",
    "orderNumberSIEBEL",
    "remark"
})
@XmlSeeAlso({
    ChangeSalesOrderRequest.class
})
public class ChangeSalesOrderRequestType {

    @XmlElement(required = true)
    protected String salesOrderNumberSAP;
    @XmlElement(required = true)
    protected String orderNumberSIEBEL;
    @XmlElement(required = true)
    protected String remark;

    /**
     * Gets the value of the salesOrderNumberSAP property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSalesOrderNumberSAP() {
        return salesOrderNumberSAP;
    }

    /**
     * Sets the value of the salesOrderNumberSAP property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSalesOrderNumberSAP(String value) {
        this.salesOrderNumberSAP = value;
    }

    /**
     * Gets the value of the orderNumberSIEBEL property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderNumberSIEBEL() {
        return orderNumberSIEBEL;
    }

    /**
     * Sets the value of the orderNumberSIEBEL property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderNumberSIEBEL(String value) {
        this.orderNumberSIEBEL = value;
    }

    /**
     * Gets the value of the remark property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemark() {
        return remark;
    }

    /**
     * Sets the value of the remark property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemark(String value) {
        this.remark = value;
    }

}
