
package hr.vipnet.xmlns.tibco.esb.crmservices;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ChangeSalesOrderResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ChangeSalesOrderResponseType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Item" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="salesOrderNumberSAP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="deliveryNumberSAP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="salesOrderSIEBEL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChangeSalesOrderResponseType", propOrder = {
    "item"
})
public class ChangeSalesOrderResponseType {

    @XmlElement(name = "Item")
    protected List<ChangeSalesOrderResponseType.Item> item;

    /**
     * Gets the value of the item property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the item property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ChangeSalesOrderResponseType.Item }
     * 
     * 
     */
    public List<ChangeSalesOrderResponseType.Item> getItem() {
        if (item == null) {
            item = new ArrayList<ChangeSalesOrderResponseType.Item>();
        }
        return this.item;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="salesOrderNumberSAP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="deliveryNumberSAP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="salesOrderSIEBEL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "salesOrderNumberSAP",
        "deliveryNumberSAP",
        "salesOrderSIEBEL"
    })
    public static class Item {

        protected String salesOrderNumberSAP;
        protected String deliveryNumberSAP;
        protected String salesOrderSIEBEL;

        /**
         * Gets the value of the salesOrderNumberSAP property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSalesOrderNumberSAP() {
            return salesOrderNumberSAP;
        }

        /**
         * Sets the value of the salesOrderNumberSAP property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSalesOrderNumberSAP(String value) {
            this.salesOrderNumberSAP = value;
        }

        /**
         * Gets the value of the deliveryNumberSAP property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDeliveryNumberSAP() {
            return deliveryNumberSAP;
        }

        /**
         * Sets the value of the deliveryNumberSAP property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDeliveryNumberSAP(String value) {
            this.deliveryNumberSAP = value;
        }

        /**
         * Gets the value of the salesOrderSIEBEL property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSalesOrderSIEBEL() {
            return salesOrderSIEBEL;
        }

        /**
         * Sets the value of the salesOrderSIEBEL property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSalesOrderSIEBEL(String value) {
            this.salesOrderSIEBEL = value;
        }

    }

}
