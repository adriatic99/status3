
package hr.vipnet.xmlns.tibco.esb.crmservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.vipnet.hr/xmlns/TIBCO/ESB/CRMServices}ChangeSalesOrderRequestType"&gt;
 *       &lt;attribute name="clientID" use="required" type="{http://www.vipnet.hr/xmlns/TIBCO/ESB/CRMServices}clientID" /&gt;
 *       &lt;attribute name="externalTransactionID" type="{http://www.vipnet.hr/xmlns/TIBCO/ESB/CRMServices}transactionID" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "ChangeSalesOrderRequest")
public class ChangeSalesOrderRequest
    extends ChangeSalesOrderRequestType
{

    @XmlAttribute(name = "clientID", required = true)
    protected String clientID;
    @XmlAttribute(name = "externalTransactionID")
    protected String externalTransactionID;

    /**
     * Gets the value of the clientID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientID() {
        return clientID;
    }

    /**
     * Sets the value of the clientID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientID(String value) {
        this.clientID = value;
    }

    /**
     * Gets the value of the externalTransactionID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalTransactionID() {
        return externalTransactionID;
    }

    /**
     * Sets the value of the externalTransactionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalTransactionID(String value) {
        this.externalTransactionID = value;
    }

}
