
package hr.vipnet.xmlns.tibco.esb.crmservices;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the hr.vipnet.xmlns.tibco.esb.crmservices package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: hr.vipnet.xmlns.tibco.esb.crmservices
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CheckSerialNumberRequestType }
     * 
     */
    public CheckSerialNumberRequestType createCheckSerialNumberRequestType() {
        return new CheckSerialNumberRequestType();
    }

    /**
     * Create an instance of {@link ConsignmentOrderRequestType }
     * 
     */
    public ConsignmentOrderRequestType createConsignmentOrderRequestType() {
        return new ConsignmentOrderRequestType();
    }

    /**
     * Create an instance of {@link CreateSalesOrderRequestType }
     * 
     */
    public CreateSalesOrderRequestType createCreateSalesOrderRequestType() {
        return new CreateSalesOrderRequestType();
    }

    /**
     * Create an instance of {@link StockCheckResponseType }
     * 
     */
    public StockCheckResponseType createStockCheckResponseType() {
        return new StockCheckResponseType();
    }

    /**
     * Create an instance of {@link GetSAPDeliveryStatusResponseType }
     * 
     */
    public GetSAPDeliveryStatusResponseType createGetSAPDeliveryStatusResponseType() {
        return new GetSAPDeliveryStatusResponseType();
    }

    /**
     * Create an instance of {@link GetSAPDeliveryStatusResponseType.Item }
     * 
     */
    public GetSAPDeliveryStatusResponseType.Item createGetSAPDeliveryStatusResponseTypeItem() {
        return new GetSAPDeliveryStatusResponseType.Item();
    }

    /**
     * Create an instance of {@link GetSAPDeliveryStatusResponseType.Item.Address }
     * 
     */
    public GetSAPDeliveryStatusResponseType.Item.Address createGetSAPDeliveryStatusResponseTypeItemAddress() {
        return new GetSAPDeliveryStatusResponseType.Item.Address();
    }

    /**
     * Create an instance of {@link CreateSalesOrderResponseType }
     * 
     */
    public CreateSalesOrderResponseType createCreateSalesOrderResponseType() {
        return new CreateSalesOrderResponseType();
    }

    /**
     * Create an instance of {@link CreateSalesOrderRequestType.CustomerData }
     * 
     */
    public CreateSalesOrderRequestType.CustomerData createCreateSalesOrderRequestTypeCustomerData() {
        return new CreateSalesOrderRequestType.CustomerData();
    }

    /**
     * Create an instance of {@link ConsignmentOrderResponseType }
     * 
     */
    public ConsignmentOrderResponseType createConsignmentOrderResponseType() {
        return new ConsignmentOrderResponseType();
    }

    /**
     * Create an instance of {@link CheckSerialNumberResponseType }
     * 
     */
    public CheckSerialNumberResponseType createCheckSerialNumberResponseType() {
        return new CheckSerialNumberResponseType();
    }

    /**
     * Create an instance of {@link ChangeSalesOrderResponseType }
     * 
     */
    public ChangeSalesOrderResponseType createChangeSalesOrderResponseType() {
        return new ChangeSalesOrderResponseType();
    }

    /**
     * Create an instance of {@link ChangeSalesOrderRequest }
     * 
     */
    public ChangeSalesOrderRequest createChangeSalesOrderRequest() {
        return new ChangeSalesOrderRequest();
    }

    /**
     * Create an instance of {@link ChangeSalesOrderRequestType }
     * 
     */
    public ChangeSalesOrderRequestType createChangeSalesOrderRequestType() {
        return new ChangeSalesOrderRequestType();
    }

    /**
     * Create an instance of {@link ChangeSalesOrderResponse }
     * 
     */
    public ChangeSalesOrderResponse createChangeSalesOrderResponse() {
        return new ChangeSalesOrderResponse();
    }

    /**
     * Create an instance of {@link ResponseStatus }
     * 
     */
    public ResponseStatus createResponseStatus() {
        return new ResponseStatus();
    }

    /**
     * Create an instance of {@link CheckSerialNumberRequest }
     * 
     */
    public CheckSerialNumberRequest createCheckSerialNumberRequest() {
        return new CheckSerialNumberRequest();
    }

    /**
     * Create an instance of {@link CheckSerialNumberRequestType.Item }
     * 
     */
    public CheckSerialNumberRequestType.Item createCheckSerialNumberRequestTypeItem() {
        return new CheckSerialNumberRequestType.Item();
    }

    /**
     * Create an instance of {@link CheckSerialNumberResponse }
     * 
     */
    public CheckSerialNumberResponse createCheckSerialNumberResponse() {
        return new CheckSerialNumberResponse();
    }

    /**
     * Create an instance of {@link ConsignmentOrderRequest }
     * 
     */
    public ConsignmentOrderRequest createConsignmentOrderRequest() {
        return new ConsignmentOrderRequest();
    }

    /**
     * Create an instance of {@link ConsignmentOrderRequestType.ConsignmentItem }
     * 
     */
    public ConsignmentOrderRequestType.ConsignmentItem createConsignmentOrderRequestTypeConsignmentItem() {
        return new ConsignmentOrderRequestType.ConsignmentItem();
    }

    /**
     * Create an instance of {@link ConsignmentOrderResponse }
     * 
     */
    public ConsignmentOrderResponse createConsignmentOrderResponse() {
        return new ConsignmentOrderResponse();
    }

    /**
     * Create an instance of {@link CreateSalesOrderRequest }
     * 
     */
    public CreateSalesOrderRequest createCreateSalesOrderRequest() {
        return new CreateSalesOrderRequest();
    }

    /**
     * Create an instance of {@link CreateSalesOrderRequestType.HWData }
     * 
     */
    public CreateSalesOrderRequestType.HWData createCreateSalesOrderRequestTypeHWData() {
        return new CreateSalesOrderRequestType.HWData();
    }

    /**
     * Create an instance of {@link CreateSalesOrderResponse }
     * 
     */
    public CreateSalesOrderResponse createCreateSalesOrderResponse() {
        return new CreateSalesOrderResponse();
    }

    /**
     * Create an instance of {@link GetSAPDeliveryStatusRequest }
     * 
     */
    public GetSAPDeliveryStatusRequest createGetSAPDeliveryStatusRequest() {
        return new GetSAPDeliveryStatusRequest();
    }

    /**
     * Create an instance of {@link GetSAPDeliveryStatusRequestType }
     * 
     */
    public GetSAPDeliveryStatusRequestType createGetSAPDeliveryStatusRequestType() {
        return new GetSAPDeliveryStatusRequestType();
    }

    /**
     * Create an instance of {@link GetSAPDeliveryStatusResponse }
     * 
     */
    public GetSAPDeliveryStatusResponse createGetSAPDeliveryStatusResponse() {
        return new GetSAPDeliveryStatusResponse();
    }

    /**
     * Create an instance of {@link StockCheckRequest }
     * 
     */
    public StockCheckRequest createStockCheckRequest() {
        return new StockCheckRequest();
    }

    /**
     * Create an instance of {@link StockCheckRequestType }
     * 
     */
    public StockCheckRequestType createStockCheckRequestType() {
        return new StockCheckRequestType();
    }

    /**
     * Create an instance of {@link StockCheckResponse }
     * 
     */
    public StockCheckResponse createStockCheckResponse() {
        return new StockCheckResponse();
    }

    /**
     * Create an instance of {@link TemplateRequest }
     * 
     */
    public TemplateRequest createTemplateRequest() {
        return new TemplateRequest();
    }

    /**
     * Create an instance of {@link TemplateRequestType }
     * 
     */
    public TemplateRequestType createTemplateRequestType() {
        return new TemplateRequestType();
    }

    /**
     * Create an instance of {@link TemplateResponse }
     * 
     */
    public TemplateResponse createTemplateResponse() {
        return new TemplateResponse();
    }

    /**
     * Create an instance of {@link TemplateResponseType }
     * 
     */
    public TemplateResponseType createTemplateResponseType() {
        return new TemplateResponseType();
    }

    /**
     * Create an instance of {@link StockCheckResponseType.WebshopItem }
     * 
     */
    public StockCheckResponseType.WebshopItem createStockCheckResponseTypeWebshopItem() {
        return new StockCheckResponseType.WebshopItem();
    }

    /**
     * Create an instance of {@link GetSAPDeliveryStatusResponseType.Item.Address.AddressItem }
     * 
     */
    public GetSAPDeliveryStatusResponseType.Item.Address.AddressItem createGetSAPDeliveryStatusResponseTypeItemAddressItem() {
        return new GetSAPDeliveryStatusResponseType.Item.Address.AddressItem();
    }

    /**
     * Create an instance of {@link CreateSalesOrderResponseType.Item }
     * 
     */
    public CreateSalesOrderResponseType.Item createCreateSalesOrderResponseTypeItem() {
        return new CreateSalesOrderResponseType.Item();
    }

    /**
     * Create an instance of {@link CreateSalesOrderRequestType.CustomerData.LegalAddress }
     * 
     */
    public CreateSalesOrderRequestType.CustomerData.LegalAddress createCreateSalesOrderRequestTypeCustomerDataLegalAddress() {
        return new CreateSalesOrderRequestType.CustomerData.LegalAddress();
    }

    /**
     * Create an instance of {@link ConsignmentOrderResponseType.Item }
     * 
     */
    public ConsignmentOrderResponseType.Item createConsignmentOrderResponseTypeItem() {
        return new ConsignmentOrderResponseType.Item();
    }

    /**
     * Create an instance of {@link CheckSerialNumberResponseType.Item }
     * 
     */
    public CheckSerialNumberResponseType.Item createCheckSerialNumberResponseTypeItem() {
        return new CheckSerialNumberResponseType.Item();
    }

    /**
     * Create an instance of {@link ChangeSalesOrderResponseType.Item }
     * 
     */
    public ChangeSalesOrderResponseType.Item createChangeSalesOrderResponseTypeItem() {
        return new ChangeSalesOrderResponseType.Item();
    }

}
