
package hr.vipnet.xmlns.tibco.esb.crmservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ResponseBody" type="{http://www.vipnet.hr/xmlns/TIBCO/ESB/CRMServices}TemplateResponseType" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="ResponseStatus" type="{http://www.vipnet.hr/xmlns/TIBCO/ESB/CRMServices}ResponseStatus" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="transactionID" use="required" type="{http://www.vipnet.hr/xmlns/TIBCO/ESB/CRMServices}transactionID" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "responseBody",
    "responseStatus"
})
@XmlRootElement(name = "TemplateResponse")
public class TemplateResponse {

    @XmlElement(name = "ResponseBody", namespace = "http://www.vipnet.hr/xmlns/TIBCO/ESB/CRMServices")
    protected TemplateResponseType responseBody;
    @XmlElement(name = "ResponseStatus", namespace = "http://www.vipnet.hr/xmlns/TIBCO/ESB/CRMServices", required = true)
    protected ResponseStatus responseStatus;
    @XmlAttribute(name = "transactionID", required = true)
    protected String transactionID;

    /**
     * Gets the value of the responseBody property.
     * 
     * @return
     *     possible object is
     *     {@link TemplateResponseType }
     *     
     */
    public TemplateResponseType getResponseBody() {
        return responseBody;
    }

    /**
     * Sets the value of the responseBody property.
     * 
     * @param value
     *     allowed object is
     *     {@link TemplateResponseType }
     *     
     */
    public void setResponseBody(TemplateResponseType value) {
        this.responseBody = value;
    }

    /**
     * Gets the value of the responseStatus property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseStatus }
     *     
     */
    public ResponseStatus getResponseStatus() {
        return responseStatus;
    }

    /**
     * Sets the value of the responseStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseStatus }
     *     
     */
    public void setResponseStatus(ResponseStatus value) {
        this.responseStatus = value;
    }

    /**
     * Gets the value of the transactionID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionID() {
        return transactionID;
    }

    /**
     * Sets the value of the transactionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionID(String value) {
        this.transactionID = value;
    }

}
