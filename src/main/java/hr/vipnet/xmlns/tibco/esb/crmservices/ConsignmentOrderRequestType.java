
package hr.vipnet.xmlns.tibco.esb.crmservices;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ConsignmentOrderRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ConsignmentOrderRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ConsignmentItem" maxOccurs="unbounded"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="VKORG" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="referenceDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="dealerCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="lineItemID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="subscriberNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="IMEI" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="storno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="enteredDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="tariffID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="tariff" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="price" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *                   &lt;element name="endPrice" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="paidAmount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="materialMaster" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="lastPriceUpdateDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="referenceIMEI" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConsignmentOrderRequestType", propOrder = {
    "consignmentItem"
})
@XmlSeeAlso({
    ConsignmentOrderRequest.class
})
public class ConsignmentOrderRequestType {

    @XmlElement(name = "ConsignmentItem", required = true)
    protected List<ConsignmentOrderRequestType.ConsignmentItem> consignmentItem;

    /**
     * Gets the value of the consignmentItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the consignmentItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getConsignmentItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ConsignmentOrderRequestType.ConsignmentItem }
     * 
     * 
     */
    public List<ConsignmentOrderRequestType.ConsignmentItem> getConsignmentItem() {
        if (consignmentItem == null) {
            consignmentItem = new ArrayList<ConsignmentOrderRequestType.ConsignmentItem>();
        }
        return this.consignmentItem;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="VKORG" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="referenceDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="dealerCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="lineItemID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="subscriberNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="IMEI" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="storno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="enteredDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="tariffID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="tariff" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="price" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
     *         &lt;element name="endPrice" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="paidAmount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="materialMaster" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="lastPriceUpdateDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="referenceIMEI" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "vkorg",
        "referenceDate",
        "dealerCode",
        "lineItemID",
        "subscriberNumber",
        "imei",
        "storno",
        "enteredDate",
        "tariffID",
        "tariff",
        "price",
        "endPrice",
        "paidAmount",
        "materialMaster",
        "lastPriceUpdateDate",
        "referenceIMEI"
    })
    public static class ConsignmentItem {

        @XmlElement(name = "VKORG")
        protected String vkorg;
        protected String referenceDate;
        protected String dealerCode;
        protected String lineItemID;
        protected String subscriberNumber;
        @XmlElement(name = "IMEI")
        protected String imei;
        protected String storno;
        protected String enteredDate;
        protected String tariffID;
        protected String tariff;
        protected BigDecimal price;
        protected String endPrice;
        protected String paidAmount;
        protected String materialMaster;
        protected String lastPriceUpdateDate;
        protected String referenceIMEI;

        /**
         * Gets the value of the vkorg property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVKORG() {
            return vkorg;
        }

        /**
         * Sets the value of the vkorg property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVKORG(String value) {
            this.vkorg = value;
        }

        /**
         * Gets the value of the referenceDate property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReferenceDate() {
            return referenceDate;
        }

        /**
         * Sets the value of the referenceDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReferenceDate(String value) {
            this.referenceDate = value;
        }

        /**
         * Gets the value of the dealerCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDealerCode() {
            return dealerCode;
        }

        /**
         * Sets the value of the dealerCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDealerCode(String value) {
            this.dealerCode = value;
        }

        /**
         * Gets the value of the lineItemID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLineItemID() {
            return lineItemID;
        }

        /**
         * Sets the value of the lineItemID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLineItemID(String value) {
            this.lineItemID = value;
        }

        /**
         * Gets the value of the subscriberNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSubscriberNumber() {
            return subscriberNumber;
        }

        /**
         * Sets the value of the subscriberNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSubscriberNumber(String value) {
            this.subscriberNumber = value;
        }

        /**
         * Gets the value of the imei property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIMEI() {
            return imei;
        }

        /**
         * Sets the value of the imei property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIMEI(String value) {
            this.imei = value;
        }

        /**
         * Gets the value of the storno property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStorno() {
            return storno;
        }

        /**
         * Sets the value of the storno property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStorno(String value) {
            this.storno = value;
        }

        /**
         * Gets the value of the enteredDate property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEnteredDate() {
            return enteredDate;
        }

        /**
         * Sets the value of the enteredDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEnteredDate(String value) {
            this.enteredDate = value;
        }

        /**
         * Gets the value of the tariffID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTariffID() {
            return tariffID;
        }

        /**
         * Sets the value of the tariffID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTariffID(String value) {
            this.tariffID = value;
        }

        /**
         * Gets the value of the tariff property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTariff() {
            return tariff;
        }

        /**
         * Sets the value of the tariff property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTariff(String value) {
            this.tariff = value;
        }

        /**
         * Gets the value of the price property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getPrice() {
            return price;
        }

        /**
         * Sets the value of the price property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setPrice(BigDecimal value) {
            this.price = value;
        }

        /**
         * Gets the value of the endPrice property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEndPrice() {
            return endPrice;
        }

        /**
         * Sets the value of the endPrice property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEndPrice(String value) {
            this.endPrice = value;
        }

        /**
         * Gets the value of the paidAmount property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPaidAmount() {
            return paidAmount;
        }

        /**
         * Sets the value of the paidAmount property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPaidAmount(String value) {
            this.paidAmount = value;
        }

        /**
         * Gets the value of the materialMaster property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMaterialMaster() {
            return materialMaster;
        }

        /**
         * Sets the value of the materialMaster property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMaterialMaster(String value) {
            this.materialMaster = value;
        }

        /**
         * Gets the value of the lastPriceUpdateDate property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLastPriceUpdateDate() {
            return lastPriceUpdateDate;
        }

        /**
         * Sets the value of the lastPriceUpdateDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLastPriceUpdateDate(String value) {
            this.lastPriceUpdateDate = value;
        }

        /**
         * Gets the value of the referenceIMEI property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReferenceIMEI() {
            return referenceIMEI;
        }

        /**
         * Sets the value of the referenceIMEI property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReferenceIMEI(String value) {
            this.referenceIMEI = value;
        }

    }

}
