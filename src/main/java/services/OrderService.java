package services;

import hr.vipnet.xmlns.tibco.esb.crmservices.GetSAPDeliveryStatusRequest;
import hr.vipnet.xmlns.tibco.esb.crmservices.GetSAPDeliveryStatusResponse;

public interface OrderService {

    GetSAPDeliveryStatusResponse GetSAPDeliveryStatus(GetSAPDeliveryStatusRequest request);
}
