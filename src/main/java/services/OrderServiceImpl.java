package services;

import hr.vipnet.xmlns.tibco.esb.crmservices.CRMWebServicesSAPSO;
import hr.vipnet.xmlns.tibco.esb.crmservices.GetSAPDeliveryStatusRequest;
import hr.vipnet.xmlns.tibco.esb.crmservices.GetSAPDeliveryStatusResponse;
import hr.vipnet.xmlns.tibco.esb.crmservices.PortType;
import interceptors.StatusInInterceptor;
import interceptors.StatusOutInterceptor;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;

@Service
public class OrderServiceImpl implements OrderService {

    private static final Logger logger = LoggerFactory.getLogger(OrderServiceImpl.class);

    @Override
    public GetSAPDeliveryStatusResponse GetSAPDeliveryStatus(GetSAPDeliveryStatusRequest request) {

        final QName SERVICE_NAME = new QName("http://www.vipnet.hr/xmlns/TIBCO/ESB/CRMServices", "CRMWebServicesSAPSO");
        final QName PORT_NAME = new QName("http://www.vipnet.hr/xmlns/TIBCO/ESB/CRMServices", "CRMWSHTTPPortSAPSO");

        final String WSDL_LOCATION = "http://esb-test.vipnet.hr:6000/CRM/CRMWebServicesSAPSO?wsdl&username=webshop&password=webshop";
        //final String WSDL_LOCATION = "C:\\a1_tomato\\HpTrackingStatus\\status2\\models\\CRMWebServicesSAPSO.wsdl";

        GetSAPDeliveryStatusResponse response = null;
        URL wsdlURL = null;
        try {
            wsdlURL = new URL(WSDL_LOCATION);
            logger.info("GetSAPDeliveryStatus");
            CRMWebServicesSAPSO service = new CRMWebServicesSAPSO(wsdlURL);
            PortType port = service.getCRMWSHTTPPortSAPSO();
            Map<String, Object> requestContext = ((BindingProvider)port).getRequestContext();
            Map<String, List<String>> requestHeaders = new HashMap<String, List<String>>();
            requestHeaders.put("ESB-Username", Collections.singletonList("webshop"));
            requestHeaders.put("ESB-Password", Collections.singletonList("webshop"));
            requestContext.put(MessageContext.HTTP_REQUEST_HEADERS, requestHeaders);
            Client cxfClient = ClientProxy.getClient(port);
            StatusOutInterceptor statusOutInter = new StatusOutInterceptor();
            StatusInInterceptor statusInInter = new StatusInInterceptor();
            cxfClient.getInInterceptors().add(statusOutInter);
            cxfClient.getInInterceptors().add(statusInInter);
            cxfClient.getInInterceptors().add(new LoggingInInterceptor());
            cxfClient.getOutInterceptors().add(new LoggingOutInterceptor());
            response = port.getSAPDeliveryStatus(request);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }
}
