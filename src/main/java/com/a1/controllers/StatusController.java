package com.a1.controllers;

import com.a1.objects.Address;
import com.a1.objects.Shipment;
import hr.vipnet.xmlns.tibco.esb.crmservices.GetSAPDeliveryStatusRequest;
import hr.vipnet.xmlns.tibco.esb.crmservices.GetSAPDeliveryStatusResponse;
import hr.vipnet.xmlns.tibco.esb.crmservices.GetSAPDeliveryStatusResponseType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import services.OrderService;
import sun.awt.image.ShortInterleavedRaster;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Controller
public class StatusController {

    @Autowired
    private OrderService orderService;

    @GetMapping("/{orderid}/{creationdate}")
    public String getStatusForOrder(@PathVariable("orderid") String orderid, @PathVariable("creationdate") String creationdate, Model model) {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        GetSAPDeliveryStatusRequest request = new GetSAPDeliveryStatusRequest();
        request.setOrderNumberSAP("2321108699");
        try {

            XMLGregorianCalendar endDate = DatatypeFactory.newInstance()
                    .newXMLGregorianCalendar(LocalDate.now().toString());
            XMLGregorianCalendar startDate = DatatypeFactory.newInstance()
                    .newXMLGregorianCalendar(creationdate);
            XMLGregorianCalendar endTime = DatatypeFactory.newInstance()
                    .newXMLGregorianCalendar();
            endTime.setTime(23,59,59);
            XMLGregorianCalendar startTime = DatatypeFactory.newInstance()
                    .newXMLGregorianCalendar("00:00:00");
            startTime.setTime(0,0,0);

            request.setDateStart(startDate);
            request.setDateEnd(endDate);
            request.setTimeEnd(endTime);
            request.setTimeStart(startTime);
            request.setClientID("Webshop");
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
        }

        model.addAttribute("orderid", orderid);
        System.out.println(orderid);

        try {
            GetSAPDeliveryStatusResponse response = orderService.GetSAPDeliveryStatus(request);
            if(response == null)
                System.out.println("response is null");
            if(response.getResponseBody() == null)
                System.out.println("body is null");
            if(response.getResponseStatus() != null)
                System.out.println(response.getResponseStatus().getMessage());
            if(response.getResponseBody().getItem() == null)
                System.out.println("item is null");
            if(response.getResponseBody().getItem().size() == 0)
                System.out.println("size 0");
            else
                System.out.println("size " + response.getResponseBody().getItem().size());

            List<Shipment> shipments = this.getListShipment(response);
            Collections.sort(shipments, new Comparator<Shipment>() {
                public int compare(Shipment o1, Shipment o2) {
                    return o2.getDatetime().compareTo(o1.getDatetime());
                }
            });
            model.addAttribute("status", shipments);
        } catch (Exception e) {
            e.printStackTrace();
            return "error";
        }

        return "status";
    }

    private List<Shipment> getListShipment(GetSAPDeliveryStatusResponse response) {

        List<Shipment> shipments = new ArrayList<>();
        try {
            List<GetSAPDeliveryStatusResponseType.Item> items = response.getResponseBody().getItem();
            for(GetSAPDeliveryStatusResponseType.Item item : items) {
                Shipment shipment = new Shipment();
                shipment.setExternalStatus(item.getExternalStatus());
                shipment.setDeliveryStatus(item.getDeliveryStatus());
                shipment.setExpressDelCom(item.getExpressDelCom());
                shipment.setHUnumberExt(item.getHUnumberExt());
                String time = item.getDate() + item.getTime();
                shipment.setDate(time.substring(6,8) + "." + time.substring(4,6) + "." + time.substring(0,4) +
                        " " + time.substring(8,10) + ":" + time.substring(10,12));
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyy HH:mm");
                LocalDateTime dateTime = LocalDateTime.parse(shipment.getDate(), formatter);
                shipment.setDatetime(dateTime);
                Address address = new Address();
                address.setName(item.getAddress().getItem().get(0).getName1());
                address.setTelNumber(item.getAddress().getItem().get(0).getTelNumber());
                address.setStreet(item.getAddress().getItem().get(0).getStreet());
                address.setTown(item.getAddress().getItem().get(0).getCity());
                address.setTelNumber(item.getAddress().getItem().get(0).getTelNumber());
                shipment.setAddress(address);
                shipments.add(shipment);
            }
        } catch(Exception e) {
            return new ArrayList<>();
        }
        return shipments;
    }
}
