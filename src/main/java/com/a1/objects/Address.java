package com.a1.objects;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Address {

    public String street;
    public String town;
    public String postNumber;
    public String name;
    public String telNumber;
}
