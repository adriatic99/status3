package com.a1.objects;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class Shipment {

    private String externalStatus;
    private String deliveryStatus;
    private String expressDelCom;
    private String date;
    private LocalDateTime datetime;
    private String HUnumberExt;
    private Address address;
}
